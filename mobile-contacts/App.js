import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import axios from "axios";
import Contact from "./components/Contact/Contact";
import {Text} from "react-native";
import {Modal} from "react-native";
import {Alert} from "react-native";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

const App = () => {
    const [contacts, setContacts] = useState([]);

    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get('https://menuredux-ponamareva-default-rtdb.firebaseio.com/contacts.json');

                setContacts(response.data);
            } catch (e) {
                console.log(e);
            }
        };

        fetchData().catch(console.error);
    }, []);

    return (
        <>
            <View style={styles.container}>
                <Text>Contacts list</Text>
                {contacts && Object.keys(contacts).map(contact => {
                    return (
                        <Contact
                            contact={contact.contact}
                            key={contacts[contact].id}
                            photo={contacts[contact].photo}
                            name={contacts[contact].name}
                            modal={() => setModalVisible(true)}
                        />
                    )
                })}
            </View>
            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setModalVisible(!modalVisible);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            {contacts && Object.keys(contacts).map(contact => {
                                return (
                                    <Contact
                                        contact={contact.contact}
                                        key={contacts[contact].id}
                                        photo={contacts[contact].photo}
                                        name={contacts[contact].name}
                                        modal={() => setModalVisible(true)}
                                    />
                                )
                            })}
                            <Pressable
                                style={[styles.button, styles.buttonClose]}
                                onPress={() => setModalVisible(!modalVisible)}
                            >
                                <Text style={styles.textStyle}>Back to list</Text>
                            </Pressable>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};

export default App;
