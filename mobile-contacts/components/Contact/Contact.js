import React from 'react';
import {StyleSheet, View, Text, Image} from "react-native";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        borderWidth: 4,
        borderRadius: 8,
        backgroundColor: 'green'
    },
    stretch: {
        width: 100,
        height: 100,
        resizeMode: 'stretch',
    },
    info: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});

const Contact = props => {
    return (
        <Pressable styles={styles.container} onPress={props.modal}>
            <View styles={styles.photo}>
                <Image
                    style={styles.stretch}
                    source={{uri: props.photo}}
                />
            </View>
            <View styles={styles.info}>
                <Text>{props.name}</Text>
            </View>
        </Pressable>
    );
};

export default Contact;