import {Switch, Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Contacts from "./containers/Contacts/Contacts";
import AddContact from "./containers/AddContact/AddContact";
import AllInfoContact from "./containers/AllInfoContact/AllInfoContact";
import EditContact from "./components/EditContact/EditContact";

import './App.css'

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Contacts} />
            <Route path="/add-contact" exact component={AddContact} />
            <Route path="/all-info/:id" exact component={AllInfoContact} />
            <Route path="/edit-contact/:id" exact component={EditContact} />
            <Route render={() => <h1>Not found</h1>}/>
        </Switch>
    </Layout>
);

export default App;
