import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import {deleteContact, fetchContacts} from "../../store/actions/contactsActions";
import Contact from "../../components/Contact/Contact";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    title: {
        paddingTop: '30px'
    }
}));

const Menu = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts.contacts);

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const deleteContactHandler = (e, id) => {
        e.preventDefault();

        dispatch(deleteContact(id));
    };

    return(
        <>
            <CssBaseline/>
            <Container>
                <Grid container spacing={4} direction="column">
                    <Grid container item direction="column" alignItems="center">
                        <Grid item xs={6} spacing={4}>
                            <Typography variant="h4" className={classes.title}>Contacts list</Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Paper>
                            {contacts.map(contact => (
                                <Contact
                                    id={contact.id}
                                    key={contact.id}
                                    photo={contact.photo}
                                    name={contact.name}
                                    delete={e => deleteContactHandler(e, contact.id)}
                                />
                            ))}
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
};

export default Menu;