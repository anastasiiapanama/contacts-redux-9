import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {addContact} from "../../store/actions/contactsActions";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {NavLink} from "react-router-dom";


const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center'
    },
    title: {
        paddingRight: '20px'
    },
    buttons: {
        alignItems: 'center',
        display: "flex",
        flexDirection: "row",
        alignContent: 'center',
        justifyContent: 'center',
    },
    button: {
        marginLeft: '25px',
        background: '#34449e',
        color: "white",
        textDecoration: "none",
        padding: '10px',
        borderRadius: '5px',
        textTransform: 'uppercase'
    }
}));

const AddContact = props => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [content, setContent] = useState({
        name: '',
        phone: '',
        email: '',
        photo: ''
    });

    const changeContent = event => {
        const {name, value} = event.target;

        setContent(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const submitContact = event => {
        event.preventDefault();

        dispatch(addContact(content, props.history))
    };

    return (
        <Paper className={classes.paper}>
            <form onSubmit={submitContact}>
                <Grid container direction="column" spacing={3}>
                    <Grid item>
                        <Typography variant="h5" align="center">Add new Dish</Typography>
                    </Grid>
                    <Grid item>
                        <strong className={classes.title}>Name:</strong>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="name"
                            label="Name"
                            value={content.name}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <strong className={classes.title}>Phone:</strong>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="phone"
                            label="Phone"
                            value={content.phone}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item className={classes.title}>
                        <strong className={classes.title}>E-mail:</strong>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="email"
                            label="Email"
                            value={content.email}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <strong className={classes.title}>Photo:</strong>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="photo"
                            label="Photo"
                            value={content.photo}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <strong className={classes.title}>Photo preview:</strong>
                        <img src={content.photo} width="100px" height="100px" alt="description of pic"/>
                    </Grid>
                    <Grid item className={classes.buttons}>
                        <Button type="submit" variant="contained" color="primary">Save</Button>
                        <NavLink to={'/'} className={classes.button}>Back to contacts</NavLink>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default AddContact;