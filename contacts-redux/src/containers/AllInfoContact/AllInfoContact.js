import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContact, fetchContacts} from "../../store/actions/contactsActions";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Contact from "../../components/Contact/Contact";
import Modal from "../../components/UI/Modal/Modal";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {NavLink} from "react-router-dom";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: '10px',
        marginBottom: '30px'
    },
    contentBlock: {
        display: 'flex',
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        marginBottom: '20px'
    },
    ModalLink: {
        textDecoration: 'none',
        fontSize: '20px',
        color: 'inherit'
    },
    image: {
        textAlign: 'center',
        paddingBottom: '20px'
    }
}));

const AllInfoContact = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts.contacts);
    const oneContact = useSelector(state => state.contacts.oneContact);

    const [oneItem, setOneItem] = useState({});

    const [modalShow, setModalHide] = useState(true);

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    useEffect(() => {
        dispatch(fetchContact(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        setOneItem(oneContact);
    }, [oneContact]);

    const modalCancelHandler = () => {
        setModalHide(false);
    };

    return(
        <>
            <CssBaseline/>
            <Container>
                <Grid container spacing={4} direction="column">
                    <Grid container item direction="column" alignItems="center">
                        <Grid item xs={6} spacing={3}>
                            <Typography variant="h4">Contacts list</Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Paper>
                            {contacts.map(contact => (
                                <Contact
                                    id={contact.id}
                                    key={contact.id}
                                    photo={contact.photo}
                                    name={contact.name}
                                />
                            ))}
                        </Paper>
                    </Grid>
                </Grid>
            </Container>

            <Modal
                show={modalShow}
                closed={modalCancelHandler}
                title="Some kinda modal title"
            >
                <Card className={classes.root} onClick={props.info}>
                    <div className={classes.contentBlock}>
                        <CardContent className={classes.content}>
                            <div className={classes.image}>
                                <img src={oneItem.photo} width="100px" height="100px" alt="description of image"/>
                            </div>
                            <Typography component="h6" variant="h6" className={classes.title}>
                                <strong>Name:</strong> {oneItem.name}
                            </Typography>
                            <Typography component="h6" variant="h6" className={classes.title}>
                                <strong>E-mail:</strong>  {oneItem.email}
                            </Typography>
                            <Typography component="h6" variant="h6" className={classes.title}>
                                <strong>Phone:</strong>  {oneItem.phone}
                            </Typography>
                        </CardContent>
                        <NavLink to='/' className={classes.ModalLink}>X</NavLink>
                    </div>
                </Card>
            </Modal>
        </>
    )
};

export default AllInfoContact;