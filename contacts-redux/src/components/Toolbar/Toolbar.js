import React from 'react';
import NavigationItems from "../Navigations/NavigationItems/NavigationItems";
import './Toolbar.css';

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <div className="Toolbar-logo">Contacts book</div>
            <nav><NavigationItems/></nav>
        </header>
    );
};

export default Toolbar;