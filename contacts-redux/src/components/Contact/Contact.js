import React from 'react';
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import Button from "@material-ui/core/Button";

const useStyle = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px',
        marginBottom: '30px'
    },
    contentBlock: {
        display: 'flex',
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        marginBottom: '20px'
    },
    showLink: {
        textDecoration: 'none',
        padding: '5px'
    },
    editLink: {
        textDecoration: 'none',
        padding: '5px 5px 10px 5px'
    }
}));

const Contact = (props) => {
    const classes = useStyle();

    return (
        <Card className={classes.root} onClick={props.info}>
            <div className={classes.contentBlock}>
                <CardMedia
                    className={classes.cover}
                    image={props.photo}
                    title="Live from space album cover"
                />
                <CardContent className={classes.content}>
                    <Typography component="h6" variant="h6" className={classes.title}>
                        <strong>Name:</strong> {props.name}
                    </Typography>
                    <NavLink to={`/all-info/${props.id}`} className={classes.showLink}>Show all info</NavLink>
                    <NavLink to={`/edit-contact/${props.id}`} className={classes.editLink}>Edit</NavLink>
                    <Button variant="contained" color="primary" onClick={props.delete}>Delete</Button>
                </CardContent>
            </div>
        </Card>
    );
};

export default Contact;
