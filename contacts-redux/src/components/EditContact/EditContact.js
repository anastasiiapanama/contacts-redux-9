import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {editContact, fetchContact} from "../../store/actions/contactsActions";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const EditContact = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const oneContact = useSelector(state => state.contacts.oneContact);

    const [oneItem, setOneItem] = useState({});

    useEffect(() => {
        dispatch(fetchContact(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        setOneItem(oneContact);
    }, [oneContact]);


    const changeContent = event => {
        const {name, value} = event.target;

        setOneItem(prev =>({
            ...prev,
            [name]: value
        }));
    };

    const submitContact = event => {
        event.preventDefault();

        dispatch(editContact(props.match.params.id, oneItem, props.history));
    };

    return (
        <Paper className={classes.paper}>
            <form onSubmit={submitContact}>
                <Grid container direction="column" spacing={3}>
                    <Grid item>
                        <Typography variant="h5" align="center">Edit contact</Typography>
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="name"
                            value={oneItem.name}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="phone"
                            value={oneItem.phone}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="email"
                            value={oneItem.email}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="photo"
                            value={oneItem.photo}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <Button type="submit" variant="contained" color="primary">Save</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default EditContact;