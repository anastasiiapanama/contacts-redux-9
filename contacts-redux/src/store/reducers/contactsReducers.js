import {ADD_CONTACT, FETCH_CONTACT_SUCCESS, FETCH_CONTACTS_SUCCESS} from "../actions/contactsActions";


const initialState = {
    contacts: [],
    oneContact: {},
};

const menuReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.contacts};
        case FETCH_CONTACT_SUCCESS:
            return {...state, oneContact: action.contact};
        case ADD_CONTACT:
            return {...state};
        default:
            return state;
    }
};

export default menuReducers;