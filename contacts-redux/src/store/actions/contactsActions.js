import axios from "axios";

export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const ADD_CONTACT = 'ADD_CONTACT';

export const fetchContactsSuccess = contacts => ({type: FETCH_CONTACTS_SUCCESS, contacts});
export const fetchContactSuccess = contact => ({type: FETCH_CONTACT_SUCCESS, contact});
export const addContactType = () => ({type: ADD_CONTACT});

export const fetchContacts = () => {
    return async dispatch => {
        const response = await axios.get('https://menuredux-ponamareva-default-rtdb.firebaseio.com/contacts.json');

        const contacts = Object.keys(response.data).map(id => ({
            ...response.data[id],
            id
        }));

        dispatch(fetchContactsSuccess(contacts));
    };
};

export const fetchContact = id => {
    return async dispatch => {
        try {
            const response = await axios.get('https://menuredux-ponamareva-default-rtdb.firebaseio.com/contacts/' + id + '.json');

            dispatch(fetchContactSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const deleteContact = id => {
    return async dispatch => {
        try {
            await axios.delete( 'https://menuredux-ponamareva-default-rtdb.firebaseio.com/contacts/' + id + ".json");

            dispatch(fetchContacts());
        } catch (e) {
            console.log(e);
        }
    };
};

export const editContact = (id, content, history) => {

    return async dispatch => {
        try {
            await axios.put(`https://menuredux-ponamareva-default-rtdb.firebaseio.com/contacts/${id}.json`, content);

            history.push('/');
        } catch (e) {
            console.log(e);
        }
    };
};

export const addContact = (data, history) => {
    return async dispatch => {
        try {
            await axios.post("https://menuredux-ponamareva-default-rtdb.firebaseio.com/contacts.json", data);

            dispatch(addContactType());
            history.push('/');
        } catch (e) {
            console.log(e);
        }
    };
};